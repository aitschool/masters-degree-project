# Motion based video summarization application
Application to summarize static video ( from surveillance camera for example ) based on the motion in it.
## How?
Because of the nature of surveillance camera being static, it is possible to detect motion in the video just by checking when the pixels in each frame change their color.

By comparing a series of pixels from a frame to the ones of the previous one it is possible to calculate "how different" a frame looks compared to the previous one.

## Installation
You first need to install the required libraries from the [ffmpeg](https://ffmpeg.org/ffmpeg.html) project.
Depending on the platform you are on, the installation could be a little different but here is a summary of the experiences I got from each platform.
- **MacOS**\
  The easiest way to install the required libraries on MacOS is to install FFMpeg though brew.\
  This can be easily done by running this command:
  
        brew install ffmpeg
  
  This command will install ffmpeg on your system but also the required 'libav' libraries that are used by the application.
  
  Another option is to compile from source code: [Compilation guide on MacOS](https://trac.ffmpeg.org/wiki/CompilationGuide/macOS)


- **Linux**\
  Only installing the required libraries from the your packet manager is an option but the names of the different packages can vary depending on your distro.\
  Once again compiling from source is also an option: \
  [Compilation guide on Ubuntu based distro](https://trac.ffmpeg.org/wiki/CompilationGuide/Ubuntu) \
  [Compilation guide on CentOS based distro](https://trac.ffmpeg.org/wiki/CompilationGuide/Centos)

  
- **Windows**\
  I don't own a Windows machine and thus never tried to run the program on it.\
  Here is a link to a [Compilation guide on Windows](https://trac.ffmpeg.org/wiki/CompilationGuide/MinGW)


- **Platform Independent**\
  Here is a link to a [Generic compilation guide](https://trac.ffmpeg.org/wiki/CompilationGuide/Generic)

Once all the required libraries are installed on your system:

      mkdir 'build'
      cmake -G "CodeBlocks - Unix Makefiles" -B ./build .
      cd build
      make
## File structure
- **src/** -> Source code
- **build/** -> Build directory
- **CMakeList.txt** -> CMake file to build the application
