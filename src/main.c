//
// Created by François Devilez on 02/01/2021.
//

#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavutil/opt.h>
#include <libswscale/swscale.h>
#include <time.h>
#include <libavfilter/avfilter.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>

// Accuracy of the color difference algorithm
#define ACCURACY 10

void closeFilters(AVFilterInOut *pFilterInputs, AVFilterInOut *pFilterOutputs, AVFilterGraph *pFilterGraph) {
    avfilter_inout_free(&pFilterInputs);
    avfilter_inout_free(&pFilterOutputs);
    avfilter_graph_free(&pFilterGraph);
    if(pFilterInputs == NULL){
        fprintf(stdout, "Filter inputs successfully closed\n");
    }
    if(pFilterOutputs == NULL){
        fprintf(stdout, "Filter outputs successfully closed\n");
    }
    if(pFilterGraph == NULL){
        printf("Filter graph successfully closed\n");
    }
}

/**
 * Close all the format contexts, codec contexts and the swsscale context
 * @param pOutputFmtCtx Format context of the output file
 * @param pInputFmtCtx Format context of the input file
 * @param pVideoEncodeCodecContext Codec context of the output file
 * @param pVideoCodecContext Codec context of the input file
 * @param pToRGBContext Swsscale context converting YUV420 to RGB pixel format
 */
void closeContexts(AVFormatContext *pOutputFmtCtx,
                   AVFormatContext *pInputFmtCtx, AVCodecContext *pVideoEncodeCodecContext,
                   AVCodecContext *pVideoCodecContext, struct SwsContext *pToRGBContext) {
    fprintf(stdout, "\n");
    //TODO Free the frame buffer

    // free the swsContexts
    sws_freeContext(pToRGBContext);
    if(pToRGBContext == NULL){
        fprintf(stdout, "Sws context successfully closed\n");
    }

    avio_closep(&pOutputFmtCtx->pb);
    if(pOutputFmtCtx->pb == NULL){
        fprintf(stdout, "AVIO context successfully closed\n");
    }

    // Closing the output context
    avformat_close_input(&pInputFmtCtx);
    avformat_close_input(&pOutputFmtCtx);
    avformat_free_context(pInputFmtCtx);
    avformat_free_context(pOutputFmtCtx);

    if(pInputFmtCtx == NULL){
        fprintf(stdout, "Input format context successfully closed\n");
    }

    if(pOutputFmtCtx == NULL){
        fprintf(stdout, "Output format context successfully closed\n");
    }

    // Closing the codec contexts
    avcodec_free_context(&pVideoCodecContext);
    avcodec_free_context(&pVideoEncodeCodecContext);

    if(pVideoCodecContext == NULL){
        fprintf(stdout, "Input video codec context successfully closed\n");
    }

    if(pVideoEncodeCodecContext == NULL){
        fprintf(stdout, "Output video codec context successfully closed\n");
    }
}

/**
 * Close the packets
 * @param inPkt Packet read while reading the input file
 * @param outPkt Packet written while writting to the output file
 */
void closePackets(AVPacket *inPkt, AVPacket *outPkt) {
    av_packet_unref(inPkt);
    av_packet_unref(outPkt);
    av_packet_free(&inPkt);
    av_packet_free(&outPkt);

    if(inPkt == NULL){
        fprintf(stdout, "Input packet successfully closed\n");
    }

    if(outPkt == NULL){
        fprintf(stdout, "Input packet successfully closed\n");
    }
}

/**
 * Close the frame and the frame buffer
 * @param frame frame used while decoding the input video
 * @param frameBuffer frame buffer used to buffer the frames
 */
void closeFrames(AVFrame *frame, AVFrame *frameBuffer, AVFrame *filteredFrame) {
    av_frame_unref(frame);
    av_frame_unref(filteredFrame);
    av_frame_free(&frame);
    av_frame_free(&filteredFrame);
    av_freep(frameBuffer);

    if(frame == NULL){
        fprintf(stdout, "Frame successfully closed\n");
    }

    if(filteredFrame == NULL){
        fprintf(stdout, "Filtered frame successfully closed\n");
    }

    if(frameBuffer == NULL){
        fprintf(stdout, "Frame buffer successfully closed\n");
    }
}

/**
 * Unref all the frames in the frame buffer
 * @param bufferSize Size of the frame buffer
 * @param frameBuffer Frame buffer
 */
void resetFrameBuffer(const int *bufferSize, AVFrame *frameBuffer) {
    // Reset buffer
    int i;
    for (i = 0; i < (*bufferSize); i++) {
        av_frame_unref(&frameBuffer[i]);
    }
}

/**
 * Shift all frame in the buffer by 1 to the left.
 * The frame at index X in the buffer will be unref and replace by the one
 * with index X+1
 * @param bufferSize Size of the frame buffer
 * @param frameBuffer Frame buffer
 */
void shiftFrameInBuffer(const int *bufferSize, AVFrame *frameBuffer) {
    // Shift all frames in the buffer by one
    int i;
    for (i = 0; i < (*bufferSize) - 1; i++) {
        av_frame_unref(&frameBuffer[i]);
        av_frame_move_ref(&frameBuffer[i], &frameBuffer[i + 1]);
    }
}

/**
 * Add the given frame in the frame buffer and return the current number of frames
 * in the buffer.
 * @param nbFrameBuffered Number of frame buffered
 * @param frameBuffer Frame buffer
 * @param newFrame New frame to be added into the buffer
 * @param bufferSize Size of the frame buffer
 * @return number of frames in the buffer
 */
int addFrameToBuffer(int nbFrameBuffered, AVFrame *frameBuffer, AVFrame *newFrame, int bufferSize) {
    fprintf(stdout, "Adding frame to buffer\n");
    // Empty array, add at index 0
    if (nbFrameBuffered == 0) {
        av_frame_move_ref(&frameBuffer[0], newFrame);
        return 1;
    } else if (nbFrameBuffered == bufferSize) { // Array full
        shiftFrameInBuffer(&bufferSize, frameBuffer);
        av_frame_move_ref(&frameBuffer[nbFrameBuffered - 1], newFrame);
        return bufferSize;
    } else {// Array not empty, add at value of nbFrameBuffered
        av_frame_move_ref(&frameBuffer[nbFrameBuffered], newFrame);
        nbFrameBuffered++;
        return nbFrameBuffered;
    }
}

/**
 * Encode a given frame into the output file
 * @param pVideoEncodeCodecContext Codec context of the input file
 * @param frame The frame to be encoded
 * @param outPkt The packet to be encoded
 * @param pOutputFmtCtx Format context of the output file
 * @param videoStreamIndex Index of the video stream to be encoded
 */
void
encodeFrame(AVCodecContext *pVideoEncodeCodecContext, AVFrame *frame, AVPacket *outPkt, AVFormatContext *pOutputFmtCtx,
            int videoStreamIndex) {
    int ret;
    // Send the frame to the encoder
    ret = avcodec_send_frame(pVideoEncodeCodecContext, frame);
    if (ret < 0) {
        fprintf(stderr, "An error occured while sending the frame to the encoder\n");
        exit(1);
    }

    while (ret >= 0) {
        // Get back the packet containing the given frame
        ret = avcodec_receive_packet(pVideoEncodeCodecContext, outPkt);

        // Something went wrong while receiving the packet from the encoder.
        // Break the loop and resume.
        // Does not seem to corrupt the destination file?
        if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
            break;
        }

        // Have to check if the framerate is different than 29.97 ?
        // This scale the time_base from the encoder context and the one
        // from the the ouput format context to match
        av_packet_rescale_ts(outPkt, pVideoEncodeCodecContext->time_base,
                             pOutputFmtCtx->streams[videoStreamIndex]->time_base);

        // Write the packet into the destination file
        ret = av_interleaved_write_frame(pOutputFmtCtx, outPkt);

        // reset the packet
        av_packet_unref(outPkt);
    }
}

/**
 * Get the red, green and blue value from the current and last frame.
 * Calculate the difference between the three channels and average it for each pixel.
 * Once done, average the red, green and blue difference compared to the number of samples
 * which is influenced by the resolution of the video and the increment set.
 * And finally average those three values to get a number between 0 and 1.
 * The higher the number, the higher the motion detected is.
 * @param frameBuffer Frame buffer
 * @param nbFrameBuffered Number of frame buffered
 * @return The average difference between all the pixel for the given frame and the previous one
 */
double calculatePixelDifference(AVFrame *lastFrame, AVFrame *currentFrame) {
    double redDiff = 0.0, greenDiff = 0.0, blueDiff = 0.0;
    uint8_t lastRed, lastGreen, lastBlue, currentRed, currentGreen, currentBlue;
    // The increment is there to increase of decrease the accuracy of the algorithm.
    // Increasing it reduce the accuracy but speed up the processing
    // Decreasing it increase the accuracy but speed down the processing
    int nbOfSample = lastFrame->width / ACCURACY * lastFrame->height / ACCURACY;
    for (int y = 0; y < lastFrame->height; y += ACCURACY) {
        for (int x = 0; x < lastFrame->width; x += ACCURACY) {
            lastRed = lastFrame->data[0][y * lastFrame->linesize[0] + x];
            lastGreen = lastFrame->data[0][y * lastFrame->linesize[0] + x + 1];
            lastBlue = lastFrame->data[0][y * lastFrame->linesize[0] + x + 2];
            currentRed = currentFrame->data[0][y * currentFrame->linesize[0] + x];
            currentGreen = currentFrame->data[0][y * currentFrame->linesize[0] + x + 1];
            currentBlue = currentFrame->data[0][y * currentFrame->linesize[0] + x + 2];
            redDiff += abs(currentRed - lastRed) / (double) 255;
            greenDiff += abs(currentGreen - lastGreen) / (double) 255;
            blueDiff += abs(currentBlue - lastBlue) / (double) 255;
        }
    }
    redDiff /= nbOfSample;
    greenDiff /= nbOfSample;
    blueDiff /= nbOfSample;
    return (redDiff + greenDiff + blueDiff) / 3;
}

/**
 * Set the parameters to the video encode context
 * @param pVideoEncodeCodecContext The codec context to add the parameters to
 * @param pVideoCodecContext The codec context to take the parameters from
 * @param framerate The framerate of the input file
 */
void setVideoEncodeParam(AVCodecContext *pVideoEncodeCodecContext, AVCodecContext *pVideoCodecContext,
                         AVRational framerate) {
    pVideoEncodeCodecContext->width = pVideoCodecContext->width;
    pVideoEncodeCodecContext->height = pVideoCodecContext->height;
    pVideoEncodeCodecContext->pix_fmt = AV_PIX_FMT_YUV420P;
    pVideoEncodeCodecContext->bit_rate = pVideoCodecContext->bit_rate;
    pVideoEncodeCodecContext->rc_buffer_size = pVideoCodecContext->rc_buffer_size;
    pVideoEncodeCodecContext->rc_max_rate = pVideoCodecContext->rc_max_rate;
    pVideoEncodeCodecContext->rc_min_rate = pVideoCodecContext->rc_min_rate;
    pVideoEncodeCodecContext->framerate = framerate;
    pVideoEncodeCodecContext->max_b_frames = 0;
    pVideoEncodeCodecContext->time_base = av_inv_q(framerate);
    pVideoEncodeCodecContext->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;

    // Set some option in the codec context of the decoder
    av_opt_set(pVideoEncodeCodecContext->priv_data, "preset", "slow", 0);
    av_opt_set(pVideoEncodeCodecContext->priv_data, "tune", "zerolatency", 0);
    av_opt_set(pVideoEncodeCodecContext->priv_data, "vprofile", "baseline", 0);
}

int encodingFunction(char *pSourceFile, char *pDstFile, int desiredFramerate, double motionThreshold, int retention) {
    int ret, i;
    int videoStreamIndex = -1;
    AVRational framerate;

    // Benchmarking
    time_t begin = time(NULL);

    // Set the log level to diplay only fatal errors
    av_log_set_level(AV_LOG_FATAL);

    // Setup the context and input format of the source file.
    AVFormatContext *pInputFmtCtx = avformat_alloc_context();
    AVInputFormat *inputFormat = av_find_input_format(pSourceFile);
    ret = avformat_open_input(&pInputFmtCtx, pSourceFile, inputFormat, NULL);
    if (ret < 0) {
        fprintf(stderr, "Could open the input format\n");
        exit(1);
    }

    // Setup the context and input format of the destination file.
    AVFormatContext *pOutputFmtCtx = NULL;
    ret = avformat_alloc_output_context2(&pOutputFmtCtx, NULL, NULL, pDstFile);
    if (ret < 0) {
        fprintf(stderr, "Could open the output format\n");
        exit(1);
    }

    // Find best video stream index and get the average framerate
    if (pInputFmtCtx->nb_streams < 0) {
        fprintf(stderr, "Could not find any streams in the source video file: %s\n", pSourceFile);
        return 1;
    } else {
        printf("Found %d streams in the source video file\n", pInputFmtCtx->nb_streams);
        ret = av_find_best_stream(pInputFmtCtx, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);
        videoStreamIndex = ret;
        if (desiredFramerate != 0) {
            framerate.num = desiredFramerate;
            framerate.den = 1;
        } else {
            framerate = pInputFmtCtx->streams[videoStreamIndex]->avg_frame_rate;
        }
    }
    if (videoStreamIndex == -1) {
        printf("No video stream index set, quitting\n");
        exit(1);
    }

    // Get the duration in sec of the input video stream
    double inputDuration = (double) pInputFmtCtx->streams[videoStreamIndex]->duration /
                           ((double) framerate.num / (double) framerate.den);

    // Get the decoder to decode the source file.
    AVCodecParameters *videoCodecParams = pInputFmtCtx->streams[videoStreamIndex]->codecpar;
    AVCodec *videoDecoder = avcodec_find_decoder(videoCodecParams->codec_id);
    if (!videoDecoder) {
        fprintf(stderr, "No decoder could be found for this video stream\n");
        exit(1);
    }

    // Setup the video codec context
    AVCodecContext *pVideoCodecContext = avcodec_alloc_context3(videoDecoder);
    ret = avcodec_parameters_to_context(pVideoCodecContext, videoCodecParams);
    if (ret < 0) {
        fprintf(stderr, "Could not give parameters to source codec context\n");
        exit(1);
    }
    ret = avcodec_open2(pVideoCodecContext, videoDecoder, NULL);
    if (ret < 0) {
        fprintf(stderr, "Could not open the video decoder using the video codec context\n");
        exit(1);
    }

    // Create streams in the destination format context to match the ones
    // in the source format context
    AVStream *out_stream = avformat_new_stream(pOutputFmtCtx, NULL);
    if (!out_stream) {
        fprintf(stderr, "Could not create a new stream in the output format context\n");
        exit(1);
    }

    // Setup the video encoder to be h264
    AVCodec *videoEncoder = avcodec_find_encoder_by_name("libx264");
    if (!videoEncoder) {
        fprintf(stderr, "Could find the h264 encoder\n");
        exit(1);
    }

    // Setuo the codec context of the encoder to mostly match the ones of the source file
    AVCodecContext *pVideoEncodeCodecContext = avcodec_alloc_context3(videoEncoder);
    setVideoEncodeParam(pVideoEncodeCodecContext, pVideoCodecContext, framerate);

    // Open the codec context and copy the configured codec context to it
    ret = avcodec_open2(pVideoEncodeCodecContext, videoEncoder, NULL);
    if (ret < 0) {
        fprintf(stderr, "Could not open the video encoder using the video encode codec context\n");
        exit(1);
    }
    ret = avcodec_parameters_from_context(out_stream->codecpar, pVideoEncodeCodecContext);
    if (ret < 0) {
        fprintf(stderr, "Could not give the codec parameters to the video encode codec context\n");
        exit(1);
    }

    // Open the destination file
    ret = avio_open(&pOutputFmtCtx->pb, pDstFile, AVIO_FLAG_WRITE);
    if (ret < 0) {
        fprintf(stderr, "Could not open the destination file\n");
        exit(1);
    }

    // Write the mp4 headers to the destination file
    ret = avformat_write_header(pOutputFmtCtx, NULL);
    if (ret < 0) {
        fprintf(stderr, "Could not write headers to the destination file\n");
        exit(1);
    }

    // Reading packet/frames from the source file and
    // encode them into the destination file
    int nbOfFrames = 0;
    AVPacket *inPkt = av_packet_alloc();
    AVPacket *outPkt = av_packet_alloc();
    AVFrame *frame = av_frame_alloc();

    // Motion detection part
    // Setup the context to convert the pixel format to RGB
    struct SwsContext *pToRGBContext = sws_getContext(
            pVideoCodecContext->width, pVideoCodecContext->height, pVideoCodecContext->pix_fmt,
            pVideoCodecContext->width, pVideoCodecContext->height, AV_PIX_FMT_RGB0,
            SWS_BILINEAR, NULL, NULL, NULL
    );

    if (!pToRGBContext) {
        fprintf(stderr, "Could not get swscontext\n");
        exit(1);
    }

    // Buffering part
    int bufferSize = (retention * framerate.num / framerate.den) + 1;
    AVFrame frameBuffer[bufferSize];
    int nbFrameBuffered = 0;
    int ptsCounter = 0;
    int encodeAfter = 0;
    double final = 0.0;

    // Filtering part
    char args[512];
    AVFilterContext *pFilterSinkCtx;
    AVFilterContext *pFilterSrcCtx;
    const AVFilter *pFilterSrc = avfilter_get_by_name("buffer");
    const AVFilter *pFilterSink = avfilter_get_by_name("buffersink");
    AVFilterInOut *pFilterOutputs = avfilter_inout_alloc();
    AVFilterInOut *pFilterInputs = avfilter_inout_alloc();
    AVRational time_base = pInputFmtCtx->streams[videoStreamIndex]->time_base;
    enum AVPixelFormat pix_fmts[] = {AV_PIX_FMT_YUV420P, AV_PIX_FMT_NONE};

    // Setup the filter to draw the current frame number and the current second onto the processed video
    const char *pFilterText = "drawtext=fontfile=arial.ttf:"
                              "fontcolor=black:"
                              "box=1:"
                              "boxcolor=white:"
                              "boxborderw=5:"
                              "fontsize=24:"
                              "x=(w-tw)-5:"
                              "y=(h-th)-5:"
                              "text='Frame\\: %{frame_num} Timecode\\:%{pts \\: hms}':"
                              "start_number=1";

    // Try to create a new filter graph holding all the nodes
    AVFilterGraph *pFilterGraph = avfilter_graph_alloc();
    if (!pFilterOutputs || !pFilterInputs || !pFilterGraph) {
        ret = AVERROR(ENOMEM);
    }

    // This give the video info into the args variable.
    // Do not remove it
    snprintf(args, sizeof(args),
             "video_size=%dx%d:pix_fmt=%d:time_base=%d/%d:pixel_aspect=%d/%d",
             pVideoCodecContext->width, pVideoCodecContext->height, pVideoCodecContext->pix_fmt,
             time_base.num, time_base.den,
             pVideoCodecContext->sample_aspect_ratio.num, pVideoCodecContext->sample_aspect_ratio.den);

    // Create the video buffer starting the filter chain
    // and give it the name "in"
    ret = avfilter_graph_create_filter(&pFilterSrcCtx, pFilterSrc, "in",
                                       args, NULL, pFilterGraph);
    if (ret < 0) {
        av_log(NULL, AV_LOG_ERROR, "Cannot create buffer source\n");
    }

    // Create the video buffer ending the filter chain
    // and give it the name "out"
    ret = avfilter_graph_create_filter(&pFilterSinkCtx, pFilterSink, "out",
                                       NULL, NULL, pFilterGraph);
    if (ret < 0) {
        av_log(NULL, AV_LOG_ERROR, "Cannot create buffer sink\n");
    }

    //Set the pixel format of sink output
    ret = av_opt_set_int_list(pFilterSinkCtx, "pix_fmts", pix_fmts,
                              AV_PIX_FMT_NONE, AV_OPT_SEARCH_CHILDREN);
    if (ret < 0) {
        av_log(NULL, AV_LOG_ERROR, "Cannot set output pixel format\n");
    }

    // Link the output of the "in" buffer to the first filter specified in the "pFilterText"
    pFilterOutputs->name = av_strdup("in");
    pFilterOutputs->filter_ctx = pFilterSrcCtx;
    pFilterOutputs->pad_idx = 0;
    pFilterOutputs->next = NULL;

    // Link the input of the "out" buffer to the last filter specified in the "pFilterText"
    pFilterInputs->name = av_strdup("out");
    pFilterInputs->filter_ctx = pFilterSinkCtx;
    pFilterInputs->pad_idx = 0;
    pFilterInputs->next = NULL;

    ret = avfilter_graph_parse_ptr(pFilterGraph, pFilterText,
                                   &pFilterInputs, &pFilterOutputs, NULL);
    if (ret < 0)
        fprintf(stderr, "Error1\n");

    ret = avfilter_graph_config(pFilterGraph, NULL);
    if (ret < 0)
        fprintf(stderr, "Error2\n");
    AVFrame *filteredFrame = av_frame_alloc();

    while (1) {
        // Send the inPkt to the decoder
        ret = av_read_frame(pInputFmtCtx, inPkt);
        if (ret < 0) {
            break;
        }

        // Skip the packet if its index is not the same as the video stream index set
        if (inPkt->stream_index != videoStreamIndex) {
            continue;
        }

        // Send the packet from the source file and get back the frame in it
        ret = avcodec_send_packet(pVideoCodecContext, inPkt);
        if (ret < 0) {
            fprintf(stderr, "An error occured while sending a packet to the decoder: %s\n", av_err2str(ret));
            exit(1);
        }

        ret = avcodec_receive_frame(pVideoCodecContext, frame);
        if (ret < 0) {
            fprintf(stderr, "An error occured while receiving a frame: %s\n", av_err2str(ret));
            exit(1);
        }

        // Converting YUV to RGB and processing pixel part
        uint8_t *pRgb = calloc(4 * frame->width * frame->height, sizeof(uint8_t));
        uint8_t *rgb[1] = {pRgb};
        int rgb_stride[1] = {4 * frame->width};

        // Fill the rgb variables with the converted rgb pixels
        ret = sws_scale(pToRGBContext, (const uint8_t *const *) frame->data, frame->linesize, 0, frame->height, rgb,
                        rgb_stride);
        if (ret < 0) {
            fprintf(stderr, "An error occured while converting pixel format\n");
            exit(1);
        }

        if (nbFrameBuffered > 1) {
            final = calculatePixelDifference(&frameBuffer[nbFrameBuffered - 2], &frameBuffer[nbFrameBuffered - 1]);
            if (final >= motionThreshold) {
                fprintf(stdout, "Motion detected: %f\n", final);
            }
        }

        // Add the current decoded frame into the filter graph
        if (av_buffersrc_add_frame_flags(pFilterSrcCtx, frame, AV_BUFFERSRC_FLAG_KEEP_REF) < 0) {
            av_log(NULL, AV_LOG_ERROR, "Error while feeding the filtergraph\n");
            break;
        }

        // Get back the filtered frame from the filter graph
        while (1) {
            ret = av_buffersink_get_frame(pFilterSinkCtx, filteredFrame);
            if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
                break;
            }
            if (ret < 0) {
                fprintf(stderr, "Error3\n");
            }
        }

        nbFrameBuffered = addFrameToBuffer(nbFrameBuffered, frameBuffer, filteredFrame, bufferSize);

        // Encoding each frame in the buffer with the correct pts
        if (final >= motionThreshold && encodeAfter == 0 && nbFrameBuffered >= bufferSize) {
            fprintf(stdout, "Encoding before and at motion\n");
            for (i = 0; i < bufferSize; i++) {
                fprintf(stdout, "Encoding frame with pts %d\n", ptsCounter);
                frameBuffer[i].pts = ptsCounter;
                ptsCounter++;
                encodeFrame(pVideoEncodeCodecContext, &frameBuffer[i], outPkt, pOutputFmtCtx, videoStreamIndex);
            }
            resetFrameBuffer(&bufferSize, frameBuffer);
            nbFrameBuffered = 0;
            encodeAfter = 1;
        }

        // Encode the frames in the buffer. Those are the ones right after a moment is being detected.
        if (encodeAfter == 1 && nbFrameBuffered >= bufferSize) {
            fprintf(stdout, "Encoding after motion\n");
            for (i = 0; i < bufferSize; i++) {
                fprintf(stdout, "Encoding frame with pts %d\n", ptsCounter);
                frameBuffer[i].pts = ptsCounter;
                ptsCounter++;
                encodeFrame(pVideoEncodeCodecContext, &frameBuffer[i], outPkt, pOutputFmtCtx, videoStreamIndex);
            }
            resetFrameBuffer(&bufferSize, frameBuffer);
            nbFrameBuffered = 0;
            encodeAfter = 0;
        }

        nbOfFrames++;

        // free the rgdbuffer
        free(pRgb);
        av_packet_unref(inPkt);
        av_packet_unref(outPkt);
        av_frame_unref(frame);
        av_frame_unref(filteredFrame);
    }

    // Write the trailer to the destination file.
    av_write_trailer(pOutputFmtCtx);

    // Close all the opened elements
    closeContexts(pOutputFmtCtx, pInputFmtCtx, pVideoCodecContext,
                  pVideoEncodeCodecContext,
                  pToRGBContext);

    // Unref all the frame in the buffer
    resetFrameBuffer(&bufferSize, frameBuffer);

    // Free the frame and the frame buffer
    closeFrames(frame, frameBuffer, filteredFrame);

    // Free the packets
    closePackets(inPkt, outPkt);

    // Free the filtering part
    closeFilters(pFilterInputs, pFilterOutputs, pFilterGraph);

    fprintf(stdout, "\n-----------------------------\n"
                    "|          STATISTICS:       |\n"
                    "-----------------------------\n"
    );
    fprintf(stdout, "Total number of frames encoded: %d\n", ptsCounter);
    fprintf(stdout, "Total number of frames decoded: %d\n", nbOfFrames);
    fprintf(stdout, "Frames kept in the output file: %.2f%%\n", ((double) ptsCounter / (double) nbOfFrames) * 100);

    // Benchmark end
    time_t end = time(NULL);
    fprintf(stdout, "\nInput stream duration: %fsec\n", inputDuration);
    fprintf(stdout, "Time it took to run app: %ld seconds\n", (end - begin));
    printf( "Processing speed ratio: %f%% seconds\n\n", (inputDuration / (double) (end - begin)) * 100);

    printf("Press ENTER key to Continue\n");
    getchar();
    getchar();

    return 0;
}

/**
 * Display the main menu
 */
void displayMenu() {
    char *pSourceFile = NULL, *pDstFile = NULL;
    int desiredFramerate = 0, choice = -1, retention = 0;
    char char_choice[5], tmp[200], srcFile[200],  dstFile[200];
    double motionThreshold = -1.00;
//    const int menuOptionsNumber = 8;
    char menuOptions[8][40] = {
            "Set source filename",
            "Set destination filename",
            "Set motion threshold",
            "Set retention time",
            "Read source file with VLC",
            "Read destination file with VLC",
            "Start encoding",
            "Quit"
    };
    char command[50];

    do {
        // Printf the options in the menuOptions variable
        printf("\e[1;1H\e[2J");
        fprintf(stdout, "-----------------------------\n"
                        "|           Menu:           |\n"
                        "-----------------------------\n\n"
                        );
        for (int k = 0; k < 8; k++) {
            switch (k) {
                case 0:
                    if(pSourceFile != NULL){
                        printf("\033[1;32m");
                        fprintf(stdout, "%d. %s - %s\n", k + 1, menuOptions[k], pSourceFile);
                        printf("\033[0m");
                    }
                    else{
                        printf("\033[1;31m");
                        printf("%d. %s\n", k + 1, menuOptions[k]);
                        printf("\033[0m");
                    }
                    break;
                case 1:
                    if(pDstFile != NULL){
                        printf("\033[1;32m");
                        fprintf(stdout, "%d. %s - %s\n", k + 1, menuOptions[k], pDstFile);
                        printf("\033[0m");
                    }
                    else{
                        printf("\033[1;31m");
                        printf("%d. %s\n", k + 1, menuOptions[k]);
                        printf("\033[0m");
                    }
                    break;
                case 2:
                    if(motionThreshold != -1.00){
                        printf("\033[1;32m");
                        fprintf(stdout, "%d. %s - %.3f\n", k + 1, menuOptions[k], motionThreshold);
                        printf("\033[0m");
                    }
                    else{
                        printf("\033[1;31m");
                        printf("%d. %s\n", k + 1, menuOptions[k]);
                        printf("\033[0m");
                    }
                    break;
                case 3:
                    if(retention != 0){
                        printf("\033[1;32m");
                        fprintf(stdout, "%d. %s - %d seconds\n", k + 1, menuOptions[k], retention);
                        printf("\033[0m");
                    }
                    else{
                        printf("\033[1;31m");
                        printf("%d. %s\n", k + 1, menuOptions[k]);
                        printf("\033[0m");
                    }
                    break;
                case 4:
                    if(pSourceFile != NULL){
                        printf("\033[1;32m");
                        fprintf(stdout, "\n%d. %s - %s\n", k + 1, menuOptions[k], pSourceFile);
                        printf("\033[0m");
                    }
                    else{
                        printf("\033[1;31m");
                        printf("\n%d. %s\n", k + 1, menuOptions[k]);
                        printf("\033[0m");
                    }
                    break;
                case 5:
                    if(pDstFile != NULL){
                        printf("\033[1;32m");
                        fprintf(stdout, "%d. %s - %s\n", k + 1, menuOptions[k], pDstFile);
                        printf("\033[0m");
                    }
                    else{
                        printf("\033[1;31m");
                        printf("%d. %s\n", k + 1, menuOptions[k]);
                        printf("\033[0m");
                    }
                    break;
                case 6:
                    if(pSourceFile != NULL && pDstFile != NULL && motionThreshold != 0.00 && retention != 0){
                        printf("\033[1;32m");
                        fprintf(stdout, "\n%d. %s : %s -> %s\n", k + 1, menuOptions[k], pSourceFile, pDstFile);
                        printf("\033[0m");
                    }
                    else{
                        printf("\033[1;31m");
                        printf("\n%d. %s\n", k + 1, menuOptions[k]);
                        printf("\033[0m");
                    }
                    break;
                default:
                    printf("\n%d. %s\n", k + 1, menuOptions[k]);
                    break;
            }
        }

        // Scan the user input and cast it to int
        scanf("%s", char_choice);
        choice = atoi(char_choice);

        switch (choice) {
            case 1:
                memset(tmp, 0, sizeof tmp);
                memset(srcFile, 0, sizeof srcFile);
                printf("\e[1;1H\e[2J");
                printf("Please input source filename without extension: ");
                scanf("%s", tmp);
                strcat(tmp, ".y4m");
                strcat(srcFile, "input/");
                strcat(srcFile, tmp);
                pSourceFile = srcFile;
                break;
            case 2:
                memset(tmp, 0, sizeof tmp);
                memset(dstFile, 0, sizeof dstFile);
                printf("\e[1;1H\e[2J");
                printf("Please input destination filename without extension: ");
                scanf("%s", tmp);
                strcat(tmp, ".mp4");
                strcat(dstFile, "output/");
                strcat(dstFile, tmp);
                pDstFile = dstFile;
                break;
            case 3:
                do{
                    printf("\e[1;1H\e[2J");
                    printf("Please input motion detection threshold ranging from 0.00 to 1.00: ");
                    scanf("%lf", &motionThreshold);
                } while (motionThreshold < 0.00 || motionThreshold > 1.00);
                motionThreshold = motionThreshold;
                break;
            case 4:
                printf("\e[1;1H\e[2J");
                printf("Please input how many seconds the program should retain before and after detecting motion: ");
                scanf("%d", &retention);
                break;
            case 5:
                snprintf(command, sizeof(command), "vlc %s", pSourceFile);
                system(command);
                break;
            case 6:
                snprintf(command, sizeof(command), "vlc %s", pDstFile);
                system(command);
                break;
            case 7:
                if (pSourceFile != NULL && pDstFile != NULL && motionThreshold >= 0.00 && retention >= 0.00) {
                    encodingFunction(pSourceFile, pDstFile, desiredFramerate, motionThreshold, retention);
                }
                break;
            case 8:
                printf("\e[1;1H\e[2J");
                break;
            default:
                printf("\033[1;31m");
                printf("Option not recognize\n\n");
                printf("\033[0m");
        }
    } while (choice != 8);
}

/**
 * Run the application
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char **argv) {
    displayMenu();
    return 0;
}